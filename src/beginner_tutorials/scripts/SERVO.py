#!/usr/bin/env python

from __future__ import division
import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import String
from std_msgs.msg import Float32
import time
import Adafruit_PCA9685
import RPi.GPIO as GPIO
import ultra
import sys


pwm0_direction = -1
pwm1_direction = 1
pwm2_direction = 1

#PWM signals for the servos
pwm0_init = 300
pwm0_range = 100
pwm0_max  = 500
pwm0_min  = 100
pwm0_pos  = pwm0_init

pwm1_init = 150
pwm1_range = 150
pwm1_max  = 450
pwm1_min  = 150
pwm1_pos  = pwm1_init

pwm2_init = 300
pwm2_range = 150
pwm2_max  = 450
pwm2_min  = 150
pwm2_pos  = pwm2_init

#pwm3_init = 300
#pwm3_range = 150
#pwm3_max  = 450
#pwm3_min  = 150
#pwm3_pos  = pwm3_init

pwm = Adafruit_PCA9685.PCA9685()
servo_min = 150
servo_max = 600

#twist is repeat in ROS
def __init__(self):
        self.twist = Twist()


def set_servo_pulse(channel, pulse):
    pulse_length = 1000000    # 1,000,000 us per second
    pulse_length //= 60       # 60 Hz
    print('{0}us per period'.format(pulse_length))
    pulse_length //= 4096     # 12 bits of resolution
    print('{0}us per bit'.format(pulse_length))
    pulse *= 1000
    pulse //= pulse_length
    pwm.set_pwm(channel, 0, pulse)
    pwm.set_pwm_freq(60)
    print('Moving servo on channel 0, as always do the ol classic Ctrl-C to quit')

def turnLeft(coe=1):
        global pwm2_pos
        pwm2_pos = pwm2_init + int(coe*pwm2_range*pwm2_direction)
        pwm2_pos = ctrl_range(pwm2_pos, pwm2_max, pwm2_min)
        #RGB.both_off()
        #RGB.yellow()
        pwm.set_pwm(2, 0, pwm2_pos)


def turnRight(coe=1):
        global pwm2_pos
        pwm2_pos = pwm2_init - int(coe*pwm2_range*pwm2_direction)
        pwm2_pos = ctrl_range(pwm2_pos, pwm2_max, pwm2_min)
        #RGB.both_off()
        #RGB.yellow()
        pwm.set_pwm(2, 0, pwm2_pos)


def turnMiddle():
        global pwm2_pos
        pwm2_pos = pwm2_init
        #RGB.both_on()
        pwm.set_pwm(2, 0, pwm2_pos)


def setPWM(num, pos):
        global pwm0_init, pwm1_init, pwm2_init, pwm0_pos, pwm1_pos, pwm2_pos
        pwm.set_pwm(num, 0, pos)
        if num == 0:
                pwm0_init = pos
                pwm0_pos = pos
        elif num == 1:
                pwm1_init = pos
                pwm1_pos = pos
        elif num == 2:
                pwm2_init = pos
                pwm2_pos = pos
def lookleft(speed):
        global pwm1_pos
        pwm1_pos += speed*pwm1_direction
#       pwm1_pos = ctrl_range(pwm1_pos, pwm1_max, pwm1_min)
        pwm.set_pwm(1, 0, pwm1_pos)


def lookright(speed):
        global pwm1_pos
        pwm1_pos -= speed*pwm1_direction
        #pwm1_pos = ctrl_range(pwm1_pos, pwm1_max, pwm1_min)
        pwm.set_pwm(1, 0, pwm1_pos)


def lookleft2(speed):
        global pwm0_pos
        pwm0_pos += speed*pwm0_direction
#       pwm1_pos = ctrl_range(pwm1_pos, pwm1_max, pwm1_min)
        pwm.set_pwm(0, 0, pwm0_pos)


def lookright2(speed):
        global pwm0_pos
        pwm0_pos -= speed*pwm0_direction
        #pwm1_pos = ctrl_range(pwm1_pos, pwm1_max, pwm1_min)
        pwm.set_pwm(0, 0, pwm0_pos)

def up(speed):
        global pwm0_pos
        pwm0_pos -= speed*pwm0_direction
       # pwm0_pos = ctrl_range(pwm0_pos, pwm0_max, pwm0_min)
       # pwm.set_pwm_freq(50)
        pwm.set_pwm(0, 0, pwm0_pos)
def down(speed):
        global pwm0_pos
        pwm0_pos += speed*pwm0_direction
       # pwm0_pos = ctrl_range(pwm0_pos, pwm0_max, pwm0_min)
       # pwm.set_pwm_freq(50)
        pwm.set_pwm(0, 0, pwm0_pos)


def servo_init():
        try:
            pwm.set_pwm(0, 0, pwm0_init)
            pwm.set_pwm(1, 0, pwm1_init)
            pwm.set_pwm(2, 0, pwm2_init)
        except:
                pass
        pwm.set_pwm(0, 0, pwm0_init)
        pwm.set_pwm(1, 0, pwm1_init)
        pwm.set_pwm(2, 0, pwm2_init)

def ahead():
        global pwm1_pos, pwm0_pos
        pwm.set_pwm(1, 0, pwm1_init)
        pwm.set_pwm(0, 0, pwm0_init)
        pwm1_pos = pwm1_init
        pwm0_pos = pwm0_init

def clean_all():
        global pwm
        pwm = Adafruit_PCA9685.PCA9685()
        pwm.set_pwm_freq(50)
        pwm.set_pwm(0, 0, 0)
        pwm.set_pwm(1, 0, 0)
        pwm.set_pwm(2, 0, 0)

def ctrl_range(raw, max_genout, min_genout):
        if raw > max_genout:
                raw_output = max_genout
        elif raw < min_genout:
                raw_output = min_genout
        else:
                raw_output = raw
        return int(raw_output)

def get_direction():
        return (pwm1_pos - pwm1_init)


def callback(data): #add self, data??
    rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data)
    print (data.linear.x)
    
    forward = data.linear.x  # i
    turn = data.angular.z  #j and l 
    headturn = data.linear.y #shift J L
  
    if data.linear.x == -0.5 and data.linear.y == 0.5:
        lookright(10)
    if data.linear.x == 0.5 and data.linear.y == 0.5:
        lookleft(10)
    if headturn > 0.5 and data.linear.x == 0: #SHIFT 
        up(10)
    if headturn < -0.5 and data.linear.x == 0: #SHIFT O
        down(10)
    if headturn == 0.5 and data.linear.x == 0: #SHIFT 
        up(10)
    if headturn == -0.5 and data.linear.x == 0: #SHIFT O
        down(10)     

    if headturn == 0 and data.linear.x == 0: #SHIFT O
        ahead()


def gotTwistCB(data):
    outdata = Velocity()
    outdata.linear = data.linear.x
    outdata.angular = data.angular.z
    pub.publish(outdata)


def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('/cmd_vel', Twist, callback)
    rospy.Subscriber("chatter", Float32, callback)
    self = Twist()
    '''
    if data.linear.x > 0.1:
        pwm.set_pwm(1, 0, servo_min)
        time.sleep(1)
        pwm.set_pwm(1, 0, servo_max)
        time.sleep(1)
    '''
    rospy.spin()


if __name__ == '__main__':
    try:
            #initial boot checks to see if connection successful
            pwm.set_pwm_freq(50)
            for i in range(0,100):
                pwm.set_pwm(0, 0, (300+i))
            for i in range(0,200):
                pwm.set_pwm(0, 0, (400-i))
            for i in range(0,100):
                pwm.set_pwm(0, 0, (200+i))
            for i in range(0,100):
                pwm.set_pwm(1, 0, (200+i))
            for i in range(0,200):
                pwm.set_pwm(1, 0, (300-i))
            for i in range(0,100):
                pwm.set_pwm(1, 0, (100+i))
#            setup()
          #  move(100, 'forward', 'no', 0.1)
          #  time.sleep(1)
          #  move(30, 'forward', 'no', 0.1)
          #  time.sleep(1)
          #  move(60, 'forward', 'no', 0.1)
          #  time.sleep(1)
          #  move(20, 'forward', 'no', 0.1)
            time.sleep(1)
            listener()
            destroy()
    except KeyboardInterrupt:
            destroy()
