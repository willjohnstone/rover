#!/usr/bin/env python


import rospy
from std_msgs.msg import Float32
import time

def callback1(data):
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
#    print(checkdist())
#    time.sleep(1)

def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("ultrasonic", Float32, callback1)
    rospy.spin()

if __name__ == '__main__':
    listener()
