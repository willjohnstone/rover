import RPi.GPIO as GPIO          
from time import sleep
import Adafruit_PCA9685

in1 = 21
in2 = 26
in3 = 16
in4 = 20
#enA = 5
#enB = 13

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BCM) #unsure about this
GPIO.setup(in1,GPIO.OUT)
GPIO.setup(in2,GPIO.OUT)
GPIO.setup(in3,GPIO.OUT)
GPIO.setup(in4,GPIO.OUT)

#GPIO.setup(enA, GPIO.OUT)
#GPIO.setup(enB, GPIO.OUT)

try:  
    while True:  
        #Controlling speed (0 = off and 255 = max speed):
        GPIO.output(in1, GPIO.HIGH)
        GPIO.output(in2, GPIO.LOW)
        GPIO.output(in3, GPIO.HIGH)
        GPIO.output(in4, GPIO.LOW)

       # sleep(2)
        #motor1GPIO = GPIO.PWM(enA, 100)
       # motor1GPIO.start(20)
       # motor2GPIO = GPIO.PWM(enB, 100)
       # motor2GPIO.start(20)
  
except KeyboardInterrupt:          # trap a CTRL+C keyboard interrupt  
    GPIO.cleanup()  
