#!/usr/bin/python
#!/usr/bin/env python

from __future__ import division
import time
import rospy
from geometry_msgs.msg import Twist
import RPi.GPIO as GPIO
import sys
import Adafruit_PCA9685
import ultra
import threading 
import random
#import RGB

'''
change this form 1 to 0 to reverse servos
'''
pwm0_direction = -1
pwm1_direction = 1
pwm2_direction = 1

pwm = Adafruit_PCA9685.PCA9685()
pwm.set_pwm_freq(50)

init_pwm0 = 300
init_pwm1 = 300
init_pwm2 = 300

pwm0_init = 300
pwm0_range = 100
pwm0_max  = 500
pwm0_min  = 100
pwm0_pos  = pwm0_init

pwm1_init = 300
pwm1_range = 150
pwm1_max  = 450
pwm1_min  = 150
pwm1_pos  = pwm1_init

pwm2_init = 300
pwm2_range = 150
pwm2_max  = 450
pwm2_min  = 150
pwm2_pos  = pwm2_init

#RGB.setup()
#RGB.cyan()
class ServoCtrl(threading.Thread):

	def __init__(self, *args, **kwargs):
		self.sc_direction = [1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1]
		self.initPos = [init_pwm0,init_pwm1,init_pwm2,init_pwm3,
						init_pwm4,init_pwm5,init_pwm6,init_pwm7,
						init_pwm8,init_pwm9,init_pwm10,init_pwm11,
						init_pwm12,init_pwm13,init_pwm14,init_pwm15]
		self.goalPos = [300,300,300,300, 300,300,300,300 ,300,300,300,300 ,300,300,300,300]
		self.nowPos  = [300,300,300,300, 300,300,300,300 ,300,300,300,300 ,300,300,300,300]
		self.bufferPos  = [300.0,300.0,300.0,300.0, 300.0,300.0,300.0,300.0 ,300.0,300.0,300.0,300.0 ,300.0,300.0,300.0,300.0]
		self.lastPos = [300,300,300,300, 300,300,300,300 ,300,300,300,300 ,300,300,300,300]
		self.ingGoal = [300,300,300,300, 300,300,300,300 ,300,300,300,300 ,300,300,300,300]
		self.maxPos  = [560,560,560,560, 560,560,560,560 ,560,560,560,560 ,560,560,560,560]
		self.minPos  = [100,100,100,100, 100,100,100,100 ,100,100,100,100 ,100,100,100,100]
		self.scSpeed = [0,0,0,0, 0,0,0,0 ,0,0,0,0 ,0,0,0,0]

		self.ctrlRangeMax = 560
		self.ctrlRangeMin = 100
		self.angleRange = 180

		'''
		scMode: 'init' 'auto' 'certain' 'quick' 'wiggle'
		'''
		self.scMode = 'auto'
		self.scTime = 2.0
		self.scSteps = 30
		
		self.scDelay = 0.037
		self.scMoveTime = 0.037

		self.goalUpdate = 0
		self.wiggleID = 0
		self.wiggleDirection = 1

		super(ServoCtrl, self).__init__(*args, **kwargs)
		self.__flag = threading.Event()
		self.__flag.clear()


	def pause(self):
		print('......................pause..........................')
		self.__flag.clear()


	def resume(self):
		print('resume')
		self.__flag.set()


	def moveInit(self):
		self.scMode = 'init'
		for i in range(0,16):
			pwm.set_pwm(i,0,self.initPos[i])
			self.lastPos[i] = self.initPos[i]
			self.nowPos[i] = self.initPos[i]
			self.bufferPos[i] = float(self.initPos[i])
			self.goalPos[i] = self.initPos[i]
		self.pause()


	def initConfig(self, ID, initInput, moveTo):
		if initInput > self.minPos[ID] and initInput < self.maxPos[ID]:
			self.initPos[ID] = initInput
			if moveTo:
				pwm.set_pwm(ID,0,self.initPos[ID])
		else:
			print('initPos Value Error.')


	def moveServoInit(self, ID):
		self.scMode = 'init'
		for i in range(0,len(ID)):
			pwm.set_pwm(ID[i], 0, self.initPos[ID[i]])
			self.lastPos[ID[i]] = self.initPos[ID[i]]
			self.nowPos[ID[i]] = self.initPos[ID[i]]
			self.bufferPos[ID[i]] = float(self.initPos[ID[i]])
			self.goalPos[ID[i]] = self.initPos[ID[i]]
		self.pause()


	def posUpdate(self):
		self.goalUpdate = 1
		for i in range(0,16):
			self.lastPos[i] = self.nowPos[i]
		self.goalUpdate = 0


	def speedUpdate(self, IDinput, speedInput):
		for i in range(0,len(IDinput)):
			self.scSpeed[IDinput[i]] = speedInput[i]


	def moveAuto(self):
		for i in range(0,16):
			self.ingGoal[i] = self.goalPos[i]

		for i in range(0, self.scSteps):
			for dc in range(0,16):
				if not self.goalUpdate:
					self.nowPos[dc] = int(round((self.lastPos[dc] + (((self.goalPos[dc] - self.lastPos[dc])/self.scSteps)*(i+1))),0))
					pwm.set_pwm(dc, 0, self.nowPos[dc])

				if self.ingGoal != self.goalPos:
					self.posUpdate()
					time.sleep(self.scTime/self.scSteps)
					return 1
			time.sleep((self.scTime/self.scSteps - self.scMoveTime))

		self.posUpdate()
		self.pause()
		return 0


	def moveCert(self):
		for i in range(0,16):
			self.ingGoal[i] = self.goalPos[i]
			self.bufferPos[i] = self.lastPos[i]

		while self.nowPos != self.goalPos:
			for i in range(0,16):
				if self.lastPos[i] < self.goalPos[i]:
					self.bufferPos[i] += self.pwmGenOut(self.scSpeed[i])/(1/self.scDelay)
					newNow = int(round(self.bufferPos[i], 0))
					if newNow > self.goalPos[i]:newNow = self.goalPos[i]
					self.nowPos[i] = newNow
				elif self.lastPos[i] > self.goalPos[i]:
					self.bufferPos[i] -= self.pwmGenOut(self.scSpeed[i])/(1/self.scDelay)
					newNow = int(round(self.bufferPos[i], 0))
					if newNow < self.goalPos[i]:newNow = self.goalPos[i]
					self.nowPos[i] = newNow

				if not self.goalUpdate:
					pwm.set_pwm(i, 0, self.nowPos[i])

				if self.ingGoal != self.goalPos:
					self.posUpdate()
					return 1
			self.posUpdate()
			time.sleep(self.scDelay-self.scMoveTime)

		else:
			self.pause()
			return 0


	def pwmGenOut(self, angleInput):
		return int(round(((self.ctrlRangeMax-self.ctrlRangeMin)/self.angleRange*angleInput),0))


	def setAutoTime(self, autoSpeedSet):
		self.scTime = autoSpeedSet


	def setDelay(self, delaySet):
		self.scDelay = delaySet


	def autoSpeed(self, ID, angleInput):
		self.scMode = 'auto'
		self.goalUpdate = 1
		for i in range(0,len(ID)):
			newGoal = self.initPos[ID[i]] + self.pwmGenOut(angleInput[i])*self.sc_direction[ID[i]]
			if newGoal>self.maxPos[ID[i]]:newGoal=self.maxPos[ID[i]]
			elif newGoal<self.minPos[ID[i]]:newGoal=self.minPos[ID[i]]
			self.goalPos[ID[i]] = newGoal
		self.goalUpdate = 0
		self.resume()


	def certSpeed(self, ID, angleInput, speedSet):
		self.scMode = 'certain'
		self.goalUpdate = 1
		for i in range(0,len(ID)):
			newGoal = self.initPos[ID[i]] + self.pwmGenOut(angleInput[i])*self.sc_direction[ID[i]]
			if newGoal>self.maxPos[ID[i]]:newGoal=self.maxPos[ID[i]]
			elif newGoal<self.minPos[ID[i]]:newGoal=self.minPos[ID[i]]
			self.goalPos[ID[i]] = newGoal
		self.speedUpdate(ID, speedSet)
		self.goalUpdate = 0
		self.resume()


	def moveWiggle(self):
		self.bufferPos[self.wiggleID] += self.wiggleDirection*self.sc_direction[self.wiggleID]*self.pwmGenOut(self.scSpeed[self.wiggleID])/(1/self.scDelay)
		newNow = int(round(self.bufferPos[self.wiggleID], 0))
		if self.bufferPos[self.wiggleID] > self.maxPos[self.wiggleID]:self.bufferPos[self.wiggleID] = self.maxPos[self.wiggleID]
		elif self.bufferPos[self.wiggleID] < self.minPos[self.wiggleID]:self.bufferPos[self.wiggleID] = self.minPos[self.wiggleID]
		self.nowPos[self.wiggleID] = newNow
		self.lastPos[self.wiggleID] = newNow
		if self.bufferPos[self.wiggleID] < self.maxPos[self.wiggleID] and self.bufferPos[self.wiggleID] > self.minPos[self.wiggleID]:
			pwm.set_pwm(self.wiggleID, 0, self.nowPos[self.wiggleID])
		else:
			self.stopWiggle()
		time.sleep(self.scDelay-self.scMoveTime)


	def stopWiggle(self):
		self.pause()
		self.posUpdate()


	def singleServo(self, ID, direcInput, speedSet):
		self.wiggleID = ID
		self.wiggleDirection = direcInput
		self.scSpeed[ID] = speedSet
		self.scMode = 'wiggle'
		self.posUpdate()
		self.resume()


	def moveAngle(self, ID, angleInput):
		self.nowPos[ID] = int(self.initPos[ID] + self.sc_direction[ID]*self.pwmGenOut(angleInput))
		if self.nowPos[ID] > self.maxPos[ID]:self.nowPos[ID] = self.maxPos[ID]
		elif self.nowPos[ID] < self.minPos[ID]:self.nowPos[ID] = self.minPos[ID]
		self.lastPos[ID] = self.nowPos[ID]
		pwm.set_pwm(ID, 0, self.nowPos[ID])


	def scMove(self):
		if self.scMode == 'init':
			self.moveInit()
		elif self.scMode == 'auto':
			self.moveAuto()
		elif self.scMode == 'certain':
			self.moveCert()
		elif self.scMode == 'wiggle':
			self.moveWiggle()


	def setPWM(self, ID, PWM_input):
		self.lastPos[ID] = PWM_input
		self.nowPos[ID] = PWM_input
		self.bufferPos[ID] = float(PWM_input)
		self.goalPos[ID] = PWM_input
		pwm.set_pwm(ID, 0, PWM_input)
		self.pause()


	def run(self):
		while 1:
			self.__flag.wait()
			self.scMove()
			pass


def replace_num(initial,new_num):   #Call this function to replace data in '.txt' file
	newline=""
	str_num=str(new_num)
	with open("%s/servo.py"%sys.path[0],"r") as f:
		for line in f.readlines():
			if(line.find(initial) == 0):
				line = initial+"%s\n"%(str_num)
			newline += line
	with open("%s/servo.py"%sys.path[0],"w") as f:
		f.writelines(newline)	#Call this function to replace data in '.txt' file


def turnLeft(coe=1):
	global pwm2_pos
	pwm2_pos = pwm2_init + int(coe*pwm2_range*pwm2_direction)
	pwm2_pos = ctrl_range(pwm2_pos, pwm2_max, pwm2_min)
	#RGB.both_off()
	#RGB.yellow()
	pwm.set_pwm(2, 0, pwm2_pos)


def turnRight(coe=1):
	global pwm2_pos
	pwm2_pos = pwm2_init - int(coe*pwm2_range*pwm2_direction)
	pwm2_pos = ctrl_range(pwm2_pos, pwm2_max, pwm2_min)
	#RGB.both_off()
	#RGB.yellow()
	pwm.set_pwm(2, 0, pwm2_pos)


def turnMiddle():
	global pwm2_pos
	pwm2_pos = pwm2_init
	RGB.both_on()
	pwm.set_pwm(2, 0, pwm2_pos)


def setPWM(num, pos):
	global pwm0_init, pwm1_init, pwm2_init, pwm0_pos, pwm1_pos, pwm2_pos
	pwm.set_pwm(num, 0, pos)
	if num == 0:
		pwm0_init = pos
		pwm0_pos = pos
	elif num == 1:
		pwm1_init = pos
		pwm1_pos = pos
	elif num == 2:
		pwm2_init = pos
		pwm2_pos = pos


def saveConfig():
	#RGB.pink()
	replace_num('pwm0_init = ',pwm0_init)
	replace_num('pwm1_init = ',pwm1_init)
	replace_num('pwm2_init = ',pwm2_init)
	#RGB.cyan()


def radar_scan():
	global pwm1_pos
	#RGB.cyan()
	scan_result = 'U: '
	scan_speed = 1
	if pwm1_direction:
		pwm1_pos = pwm1_max
		pwm.set_pwm(1, 0, pwm1_pos)
		time.sleep(0.5)
		scan_result += str(ultra.checkdist())
		scan_result += ' '
		while pwm1_pos>pwm1_min:
			pwm1_pos-=scan_speed
			pwm.set_pwm(1, 0, pwm1_pos)
			scan_result += str(ultra.checkdist())
			scan_result += ' '
		pwm.set_pwm(1, 0, pwm1_init)
		pwm1_pos = pwm1_init
	else:
		pwm1_pos = pwm1_min
		pwm.set_pwm(1, 0, pwm1_pos)
		time.sleep(0.5)
		scan_result += str(ultra.checkdist())
		scan_result += ' '
		while pwm1_pos<pwm1_max:
			pwm1_pos+=scan_speed
			pwm.set_pwm(1, 0, pwm1_pos)
			scan_result += str(ultra.checkdist())
			scan_result += ' '
		pwm.set_pwm(1, 0, pwm1_init)
		pwm1_pos = pwm1_init
#	RGB.both_on()
	return scan_result


def ctrl_range(raw, max_genout, min_genout):
	if raw > max_genout:
		raw_output = max_genout
	elif raw < min_genout:
		raw_output = min_genout
	else:
		raw_output = raw
	return int(raw_output)


def lookleft(speed):
	global pwm1_pos
	pwm1_pos += speed*pwm1_direction
	pwm1_pos = ctrl_range(pwm1_pos, pwm1_max, pwm1_min)
	pwm.set_pwm(1, 0, pwm1_pos)


def lookright(speed):
	global pwm1_pos
	pwm1_pos -= speed*pwm1_direction
	pwm1_pos = ctrl_range(pwm1_pos, pwm1_max, pwm1_min)
	pwm.set_pwm(1, 0, pwm1_pos)


def up(speed):
	global pwm0_pos
	pwm0_pos -= speed*pwm0_direction
	pwm0_pos = ctrl_range(pwm0_pos, pwm0_max, pwm0_min)
	pwm.set_pwm(0, 0, pwm0_pos)


def down(speed):
	global pwm0_pos
	pwm0_pos += speed*pwm0_direction
	pwm0_pos = ctrl_range(pwm0_pos, pwm0_max, pwm0_min)
	pwm.set_pwm(0, 0, pwm0_pos)


def servo_init():
	try:
		pwm.set_all_pwm(0, 300)
	except:
		pass
	pwm.set_pwm(0, 0, pwm0_init)
	pwm.set_pwm(1, 0, pwm1_init)
	pwm.set_pwm(2, 0, pwm2_init)


def clean_all():
	global pwm
	pwm = Adafruit_PCA9685.PCA9685()
	pwm.set_pwm_freq(50)
	pwm.set_all_pwm(0, 0)


def ahead():
	global pwm1_pos, pwm0_pos
	pwm.set_pwm(1, 0, pwm1_init)
	pwm.set_pwm(0, 0, pwm0_init)
	pwm1_pos = pwm1_init
	pwm0_pos = pwm0_init


def get_direction():
	return (pwm1_pos - pwm1_init)

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data)
    print (data.linear.x)
    
    pwm.set_pwm(0, 0, pos_min)
    time.sleep(1)
    pwm.set_pwm(0, 0, pos_max)
    time.sleep(1) 
    '''
    while 1:
            for i in range(0,100):
                    pwm.set_pwm(0, 0, (300+i))
                    time.sleep(0.05)
            for i in range(0,100):
                    pwm.set_pwm(0, 0, (400-i))
                    time.sleep(0.05)

'''

def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('/cmd_vel', Twist, callback)


if __name__ == '__main__':
        sc = ServoCtrl()
        sc.start()
        while 1:
                sc.moveAngle(0,(random.random()*100-50))
                time.sleep(1)
                sc.moveAngle(1,(random.random()*100-50))
                time.sleep(1)
'''
        while 1:
		for i in range(0,100):
			pwm.set_pwm(0, 0, (300+i))
			time.sleep(0.05)
		for i in range(0,100):
			pwm.set_pwm(0, 0, (400-i))
			time.sleep(0.05)
'''
