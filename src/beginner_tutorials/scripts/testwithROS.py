#!/usr/bin/env python3
from __future__ import division
import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import String
import time
import Adafruit_PCA9685

pwm = Adafruit_PCA9685.PCA9685()

servo_min = 150  # Min pulse length out of 4096
servo_max = 600  # Max pulse length out of 4096

def __init__(self):
        self.twist = Twist()

def set_servo_pulse(channel, pulse):
    pulse_length = 1000000    # 1,000,000 us per second
    pulse_length //= 60       # 60 Hz
    print('{0}us per period'.format(pulse_length))
    pulse_length //= 4096     # 12 bits of resolution
    print('{0}us per bit'.format(pulse_length))
    pulse *= 1000
    pulse //= pulse_length
    pwm.set_pwm(channel, 0, pulse)
    pwm.set_pwm_freq(60)
    print('Moving servo on channel 0, as always do the ol classic Ctrl-C to quit')


def destroy():
        motorStop()
        GPIO.cleanup()  

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data)
    print (data.linear.x)
    if data.angular.y < 0.1:
        pwm.set_pwm(1, 0, servo_min)
        time.sleep(1)
        pwm.set_pwm(1, 0, servo_max)
        time.sleep(1)
    

def gotTwistCB(data): #when Twist msg is received
#take values from data and do your job, for example:
    outdata = Velocity()
    outdata.linear = data.linear.x
    outdata.angular = data.angular.z
    pub.publish(outdata)

def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('/cmd_vel', Twist, callback)
    self = Twist()
    '''
    if data.linear.x > 0.1:
        pwm.set_pwm(1, 0, servo_min)
        time.sleep(1)
        pwm.set_pwm(1, 0, servo_max)
        time.sleep(1)
    '''
    rospy.spin()


if __name__ == '__main__':
    try:
            pwm.set_pwm(1, 0, servo_min)
            time.sleep(1)
            pwm.set_pwm(1, 0, servo_max)
            time.sleep(1)
            listener()
            destroy()
    except KeyboardInterrupt:
            destroy()

