
from __future__ import division
import rospy
import time
import Adafruit_PCA9685
import RPi.GPIO as GPIO
import ultra
import sys
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)


pwm = Adafruit_PCA9685.PCA9685()
pwm.set_pwm(3, 0, 300)



GPIO.setup(7, GPIO.OUT)
p = GPIO.PWM(7, 50)

p.start(0)
time.sleep(3)
p.ChangeDutyCycle(5.3)
time.sleep(5)

while True:
	i = 4
	while i<10:
		
		print(i)
		p.ChangeDutyCycle(i)
		time.sleep(.05)
		i +=.02
	
	while i>4:
		print(i)
		p.ChangeDutyCycle(i)
		time.sleep(.05)
		i -=.05
