#!/usr/bin/env python

from __future__ import division
import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import String
import time
import Adafruit_PCA9685
import RPi.GPIO as GPIO
import ultra
import sys

pwm0_direction = -1
pwm1_direction = 1
pwm2_direction = 1


pwm0_init = 300
pwm0_range = 100
pwm0_max  = 500
pwm0_min  = 100
pwm0_pos  = pwm0_init

pwm1_init = 300
pwm1_range = 150
pwm1_max  = 450
pwm1_min  = 150
pwm1_pos  = pwm1_init

pwm2_init = 300
pwm2_range = 150
pwm2_max  = 450
pwm2_min  = 150
pwm2_pos  = pwm2_init

pwm3_init = 300
pwm3_range = 150
pwm3_max  = 450
pwm3_min  = 150
pwm3_pos  = pwm3_init

Motor_A_EN    = 4
Motor_B_EN    = 17

Motor_A_Pin1  = 14
Motor_A_Pin2  = 15
Motor_B_Pin1  = 27
Motor_B_Pin2  = 18

Dir_forward   = 0
Dir_backward  = 1

left_forward  = 0
left_backward = 1

right_forward = 0
right_backward= 1

pwm = Adafruit_PCA9685.PCA9685()

servo_min = 150  # Min pulse length out of 4096
servo_max = 600  # Max pulse length out of 4096

def __init__(self):
        self.twist = Twist()

def set_servo_pulse(channel, pulse):
    pulse_length = 1000000    # 1,000,000 us per second
    pulse_length //= 60       # 60 Hz
    print('{0}us per period'.format(pulse_length))
    pulse_length //= 4096     # 12 bits of resolution
    print('{0}us per bit'.format(pulse_length))
    pulse *= 1000
    pulse //= pulse_length
    pwm.set_pwm(channel, 0, pulse)
    pwm.set_pwm_freq(60)
    print('Moving servo on channel 0, as always do the ol classic Ctrl-C to quit')

def turnLeft(coe=1):
	global pwm2_pos
	pwm2_pos = pwm2_init + int(coe*pwm2_range*pwm2_direction)
	pwm2_pos = ctrl_range(pwm2_pos, pwm2_max, pwm2_min)
	#RGB.both_off()
	#RGB.yellow()
	pwm.set_pwm(2, 0, pwm2_pos)


def turnRight(coe=1):
	global pwm2_pos
	pwm2_pos = pwm2_init - int(coe*pwm2_range*pwm2_direction)
	pwm2_pos = ctrl_range(pwm2_pos, pwm2_max, pwm2_min)
	#RGB.both_off()
	#RGB.yellow()
	pwm.set_pwm(2, 0, pwm2_pos)


def turnMiddle():
	global pwm2_pos
	pwm2_pos = pwm2_init
	#RGB.both_on()
	pwm.set_pwm(2, 0, pwm2_pos)


def setPWM(num, pos):
	global pwm0_init, pwm1_init, pwm2_init, pwm0_pos, pwm1_pos, pwm2_pos
	pwm.set_pwm(num, 0, pos)
	if num == 0:
		pwm0_init = pos
		pwm0_pos = pos
	elif num == 1:
		pwm1_init = pos
		pwm1_pos = pos
	elif num == 2:
		pwm2_init = pos
		pwm2_pos = pos

def lookleft(speed):
	global pwm1_pos
	pwm1_pos += speed*pwm1_direction
	pwm1_pos = ctrl_range(pwm1_pos, pwm1_max, pwm1_min)
	pwm.set_pwm(1, 0, pwm1_pos)


def lookright(speed):
	global pwm1_pos
	pwm1_pos -= speed*pwm1_direction
	pwm1_pos = ctrl_range(pwm1_pos, pwm1_max, pwm1_min)
	pwm.set_pwm(1, 0, pwm1_pos)


def up(speed):
	global pwm0_pos
	pwm0_pos -= speed*pwm0_direction
	pwm0_pos = ctrl_range(pwm0_pos, pwm0_max, pwm0_min)
	pwm.set_pwm(0, 0, pwm0_pos)


def down(speed):
	global pwm0_pos
	pwm0_pos += speed*pwm0_direction
	pwm0_pos = ctrl_range(pwm0_pos, pwm0_max, pwm0_min)
	pwm.set_pwm(0, 0, pwm0_pos)


def servo_init():
	try:
		pwm.set_all_pwm(0, 300)
	except:
		pass
	pwm.set_pwm(0, 0, pwm0_init)
	pwm.set_pwm(1, 0, pwm1_init)
	pwm.set_pwm(2, 0, pwm2_init)


def clean_all():
	global pwm
	pwm = Adafruit_PCA9685.PCA9685()
	pwm.set_pwm_freq(50)
	pwm.set_all_pwm(0, 0)


def ahead():
	global pwm1_pos, pwm0_pos
	pwm.set_pwm(1, 0, pwm1_init)
	pwm.set_pwm(0, 0, pwm0_init)
	pwm1_pos = pwm1_init
	pwm0_pos = pwm0_init

def ctrl_range(raw, max_genout, min_genout):
	if raw > max_genout:
		raw_output = max_genout
	elif raw < min_genout:
		raw_output = min_genout
	else:
		raw_output = raw
	return int(raw_output)

def get_direction():
	return (pwm1_pos - pwm1_init)

def motorStop():#Motor stops
        GPIO.output(Motor_A_Pin1, GPIO.LOW)
        GPIO.output(Motor_A_Pin2, GPIO.LOW)
        GPIO.output(Motor_B_Pin1, GPIO.LOW)
        GPIO.output(Motor_B_Pin2, GPIO.LOW)
        GPIO.output(Motor_A_EN, GPIO.LOW)
        GPIO.output(Motor_B_EN, GPIO.LOW)


def setup():#Motor initialization
        global pwm_A, pwm_B
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(Motor_A_EN, GPIO.OUT)
        GPIO.setup(Motor_B_EN, GPIO.OUT)
        GPIO.setup(Motor_A_Pin1, GPIO.OUT)
        GPIO.setup(Motor_A_Pin2, GPIO.OUT)
        GPIO.setup(Motor_B_Pin1, GPIO.OUT)
        GPIO.setup(Motor_B_Pin2, GPIO.OUT)

        motorStop()
        try:
                pwm_A = GPIO.PWM(Motor_A_EN, 1000)
                pwm_B = GPIO.PWM(Motor_B_EN, 1000)
        except:
                pass


def motor_left(status, direction, speed):#Motor 2 positive and negative rotation
        if status == 0: # stop
                GPIO.output(Motor_B_Pin1, GPIO.LOW)
                GPIO.output(Motor_B_Pin2, GPIO.LOW)
                GPIO.output(Motor_B_EN, GPIO.LOW)
        else:
                if direction == Dir_backward:
                        GPIO.output(Motor_B_Pin1, GPIO.HIGH)
                        GPIO.output(Motor_B_Pin2, GPIO.LOW)
                        pwm_B.start(100)
                        pwm_B.ChangeDutyCycle(speed)
                elif direction == Dir_forward:
                        GPIO.output(Motor_B_Pin1, GPIO.LOW)
                        GPIO.output(Motor_B_Pin2, GPIO.HIGH)
                        pwm_B.start(0)
                        pwm_B.ChangeDutyCycle(speed)


def motor_right(status, direction, speed):#Motor 1 positive and negative rotation
        if status == 0: # stop
                GPIO.output(Motor_A_Pin1, GPIO.LOW)
                GPIO.output(Motor_A_Pin2, GPIO.LOW)
                GPIO.output(Motor_A_EN, GPIO.LOW)
        else:
                if direction == Dir_forward:#
                        GPIO.output(Motor_A_Pin1, GPIO.HIGH)
                        GPIO.output(Motor_A_Pin2, GPIO.LOW)
                        pwm_A.start(100)
                        pwm_A.ChangeDutyCycle(speed)
                elif direction == Dir_backward:
                        GPIO.output(Motor_A_Pin1, GPIO.LOW)
                        GPIO.output(Motor_A_Pin2, GPIO.HIGH)
                        pwm_A.start(0)
                        pwm_A.ChangeDutyCycle(speed)
        return direction


def move(speed, direction, turn, radius=0.6):   # 0 < radius <= 1  
        #speed = 100
        if direction == 'forward':
                if turn == 'right':
                        motor_left(0, left_backward, int(speed*radius))
                        motor_right(1, right_forward, speed)
                elif turn == 'left':
                        motor_left(1, left_forward, speed)
                        motor_right(0, right_backward, int(speed*radius))
                else:
                        motor_left(1, left_forward, speed)
                        motor_right(1, right_forward, speed)
        elif direction == 'backward':
                if turn == 'right':
                        motor_left(0, left_forward, int(speed*radius))
                        motor_right(1, right_backward, speed)
                elif turn == 'left':
                        motor_left(1, left_backward, speed)
                        motor_right(0, right_forward, int(speed*radius))
                else:
                        motor_left(1, left_backward, speed)
                        motor_right(1, right_backward, speed)
        elif direction == 'no':
                if turn == 'right':
                        motor_left(1, left_backward, speed)
                        motor_right(1, right_forward, speed)
                elif turn == 'left':
                        motor_left(1, left_forward, speed)
                        motor_right(1, right_backward, speed)
                else:
                        motorStop()
        else:
                pass

'''
def CamsMotorThingy():
    pwm.set_pwm(3, 1024, 3072)
    time.sleep(2)
    pwm.set_pwm(3, 4096, 0)
    time.sleep(2)
    pwm.set_pwm(3, 0, 4096) 
'''

def destroy():
        motorStop()
        GPIO.cleanup()             # Release resource

def callback(data): #add self, data??
    rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data)
    print (data.linear.x)
    
    forward = data.linear.x
    turn = data.angular.z 
    headturn = data.linear.y #shift J L
    if turn > 0:
        lookright(1)
       # turnMiddle()
        clean_all()
       
    elif turn < 0:
        lookleft(1) 
       # turnMiddle()
        clean_all()
    
    if headturn > 0 and forward == 0:
        turnLeft()
    if headturn < 0 and forward == 0:
        turnRight()    

    if data.linear.x == -0.5 and data.linear.y == -0.5:
        up(1)
    if data.linear.x == -0.5 and data.linear.y == 0.5:
        down(1)


    else:
        if forward>0:
            move(data.linear.x*120, 'forward', 'no', 0.8)
            turnMiddle()
 #          CamsMotorThingy()
        elif forward<0:
            move(data.linear.x*120, 'backward', 'no', 0.8)
        elif data.linear.x == 0 and data.linear.y == 0 and data.linear.z == 0 and data.angular.z==0 and data.angular.x==0 and data.angular.y ==0:
            motorStop()
        

def gotTwistCB(data): #when Twist msg is received
#take values from data and do your job, for example:
    outdata = Velocity()
    outdata.linear = data.linear.x
    outdata.angular = data.angular.z
    pub.publish(outdata)


def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('/cmd_vel', Twist, callback)
    self = Twist()
    '''
    if data.linear.x > 0.1:
        pwm.set_pwm(1, 0, servo_min)
        time.sleep(1)
        pwm.set_pwm(1, 0, servo_max)
        time.sleep(1)
    '''
    rospy.spin()


if __name__ == '__main__':
    try:
            #pwm.set_pwm(1, 0, servo_min)
            #time.sleep(1)
            #pwm.set_pwm(1, 0, servo_max)
            #time.sleep(1)
            y = 60
            setup() 
            move(y, 'forward', 'no', 0.8)
            time.sleep(1.3)
            motorStop()

            listener()
            destroy()
    except KeyboardInterrupt:
            destroy()
