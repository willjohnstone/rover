#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export PWD='/home/ubuntu/GitCatkin_ws/rover/build'
export ROS_PACKAGE_PATH='/home/ubuntu/catkin_ws/src:/home/ubuntu/GitCatkin_ws/rover/src:/home/ubuntu/catkin_ws/src:/opt/ros/noetic/share'